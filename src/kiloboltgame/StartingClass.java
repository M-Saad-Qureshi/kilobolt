package kiloboltgame;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;

import kiloboltgame.characters.Robot;

/**
 * Created by Xingsofti5 on 19-Aug-15.
 */
public class StartingClass extends Applet implements Runnable, KeyListener
{
	private Robot robot;
	private Image image, currentSprite, character, characterDown, characterJumped, background;
	private Graphics second;
	private URL base;
	private static Background bg1, bg2;

	@Override public void init()
	{
		setSize(800, 480);
		setBackground(Color.BLACK);
		setFocusable(true);
		addKeyListener(this);
		Frame frame = (Frame) this.getParent().getParent();
		frame.setTitle("Q-Bot Alpha");
		try
		{

			// Image Setups

			// Image Setups
			base = StartingClass.class.getResource("/data/character.png");
			character = getImage(base);

			base = StartingClass.class.getResource("/data/down.png");
			characterDown = getImage(base);

			base = StartingClass.class.getResource("/data/jumped.png");
			characterJumped = getImage(base);

			currentSprite = character;

			base = StartingClass.class.getResource("/data/background.png");
			background = getImage(base);
		}
		catch (Exception e)
		{
			// TODO: handle exception
		}

	}

	@Override public void start()
	{
		bg1 = new Background(0, 0);
		bg2 = new Background(2160, 0);
		robot = new Robot();

		Thread thread = new Thread(this);
		thread.start();
	}

	@Override public void update(Graphics g)
	{
		if (image == null)
		{
			image = createImage(this.getWidth(), this.getHeight());
			second = image.getGraphics();
		}

		second.setColor(getBackground());
		second.fillRect(0, 0, getWidth(), getHeight());
		second.setColor(getForeground());
		paint(second);

		g.drawImage(image, 0, 0, this);

	}

	@Override public void paint(Graphics g)
	{
		g.drawImage(background, bg1.getBgX(), bg1.getBgY(), this);
		g.drawImage(background, bg2.getBgX(), bg2.getBgY(), this);
		g.drawImage(currentSprite, robot.getCenterX() - 61, robot.getCenterY() - 63, this);

	}

	@Override public void stop()
	{
		// TODO Auto-generated method stub
		super.stop();
	}

	@Override public void destroy()
	{
		// TODO Auto-generated method stub
		super.destroy();
	}

	@Override public void run()
	{
		while (true)
		{
			robot.update();
			if (robot.isJumped())
			{
				currentSprite = characterJumped;
			}
			else if (robot.isJumped() == false && robot.isDucked() == false)
			{
				currentSprite = character;
			}
			bg1.update();
			bg2.update();
			repaint();
			try
			{
				Thread.sleep(17);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override public void keyTyped(KeyEvent e)
	{

	}

	@Override public void keyPressed(KeyEvent e)
	{

		switch (e.getKeyCode())
		{
		case KeyEvent.VK_UP:
			System.out.println("Move up");
			break;

		case KeyEvent.VK_DOWN:
			currentSprite = characterDown;
			if (robot.isJumped() == false)
			{
				robot.setDucked(true);
				robot.setSpeedX(0);
			}
			break;

		case KeyEvent.VK_LEFT:
			robot.moveLeft();
			robot.setMovingLeft(true);
			break;

		case KeyEvent.VK_RIGHT:
			robot.moveRight();
			robot.setMovingRight(true);
			break;

		case KeyEvent.VK_SPACE:
			robot.jump();
			break;

		}

	}

	@Override public void keyReleased(KeyEvent e)
	{
		switch (e.getKeyCode())
		{
		case KeyEvent.VK_UP:
			System.out.println("Stop moving up");
			break;

		case KeyEvent.VK_DOWN:
			currentSprite = character;
			robot.setDucked(false);
			break;

		case KeyEvent.VK_LEFT:
			robot.stopLeft();
			break;

		case KeyEvent.VK_RIGHT:
			robot.stopRight();
			break;

		case KeyEvent.VK_SPACE:
			break;

		}

	}

	public static Background getBg1()
	{
		return bg1;
	}

	public static Background getBg2()
	{
		return bg2;
	}
}
